package com.globtrotter.globtrotter.api;

import com.globtrotter.globtrotter.model.PurchasedTrip;
import com.globtrotter.globtrotter.service.PurchasedTripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/purchasedTrip")
public class PurchasedTripApi {

    private PurchasedTripService purchasedTripService;

    @Autowired
    public PurchasedTripApi(@RequestBody PurchasedTripService purchasedTripService) {
        this.purchasedTripService = purchasedTripService;
    }

    @CrossOrigin(value = "http://localhost:4200")
    @PostMapping
    public PurchasedTrip addPurchasedTrip(PurchasedTrip purchasedTrip) {
        return purchasedTripService.addPurchasedTrip(purchasedTrip);
    }
}
