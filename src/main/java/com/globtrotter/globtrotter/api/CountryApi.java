package com.globtrotter.globtrotter.api;

import com.globtrotter.globtrotter.api.model.CountryApiModel;
import com.globtrotter.globtrotter.model.Continent;
import com.globtrotter.globtrotter.model.Country;
import com.globtrotter.globtrotter.service.ContinentService;
import com.globtrotter.globtrotter.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/country")
public class CountryApi {

    private CountryService countryService;
    private ContinentService continentService;

    @Autowired
    public CountryApi(CountryService countryService, ContinentService continentService) {

        this.countryService = countryService;
        this.continentService = continentService;
    }

    @CrossOrigin(value = "http://localhost:4200")
    @PostMapping
    public Country addCountry(@RequestBody CountryApiModel countryApiModel) {
        Continent continent = continentService.findContinentById(countryApiModel.getContinentId());
        Country country = new Country();
        country.setId(countryApiModel.getId());
        country.setName(countryApiModel.getName());
        country.setContinent(continent);
        return countryService.addCountry(country);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @PutMapping
    public Country modifyCountry(@RequestBody CountryApiModel countryApiModel) {
        Continent continent = continentService.findContinentById(countryApiModel.getContinentId());
        Country country = new Country();
        country.setId(countryApiModel.getId());
        country.setName(countryApiModel.getName());
        country.setContinent(continent);
        return countryService.modifyCountry(country);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/all")
    public List<Country> getAllCountries() {
        return countryService.getAllCountries();
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/findByContinent")
    public List<Country> findCountryByContinent(@RequestParam String continentName) {
        return countryService.findCountriesByContinent(continentName);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/findById/{id}")
    public Country findCountryById(@PathVariable Long id) {
        return countryService.findCountryById(id);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public boolean deleteCountry(@PathVariable Long id) {
        return countryService.deleteCountry(id);
    }

}
