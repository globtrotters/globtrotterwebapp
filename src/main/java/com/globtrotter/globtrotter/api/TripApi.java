package com.globtrotter.globtrotter.api;


import com.globtrotter.globtrotter.model.Trip;
import com.globtrotter.globtrotter.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/trip")
public class TripApi {

    private TripService tripService;

    @Autowired
    public TripApi(TripService tripService) {
        this.tripService = tripService;
    }

    @CrossOrigin(value = "http://localhost:4200")
    @PostMapping
    public Trip addTrip(@RequestBody Trip trip) {

        return tripService.addTrip(trip);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/all")
    public List<Trip> getAllTrips(){
        return tripService.getAllTrips();
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/findByDepartureAirport")
    public List<Trip> getTripsByDepartureAirport(@RequestParam String airportName){
        return tripService.getTripsByDepartureAirport(airportName);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public boolean deleteTrip(@PathVariable Long id) {
        return tripService.deleteTrip(id);
    }
}
