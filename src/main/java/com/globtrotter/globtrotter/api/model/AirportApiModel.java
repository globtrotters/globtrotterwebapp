package com.globtrotter.globtrotter.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirportApiModel {

    private Long id;
    private String name;
    private Long cityId;
}
