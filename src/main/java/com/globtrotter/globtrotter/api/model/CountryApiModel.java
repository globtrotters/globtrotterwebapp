package com.globtrotter.globtrotter.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CountryApiModel {

    private Long id;
    private String name;
    private Long continentId;

}
