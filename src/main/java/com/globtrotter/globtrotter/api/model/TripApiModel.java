package com.globtrotter.globtrotter.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TripApiModel {
    private Long id;
    private Long airportFromId;
    private Long airportToId;
    private Long hotelId;
    private Long cityId;
    //TODO jak ugryźć daty?
    private Long departureDate;
    private Long returnDate;
    private Long countOfDays;
    //TODO co z tym enumem?
    private String type;
    private Long priceForAdult;
    private Long priceForChild;
    private Long promotion;
    private Long countOfPerson;
    private String description;
}
