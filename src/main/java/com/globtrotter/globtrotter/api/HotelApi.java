package com.globtrotter.globtrotter.api;

import com.globtrotter.globtrotter.model.Hotel;
import com.globtrotter.globtrotter.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/hotel")
public class HotelApi {

    private HotelService hotelService;

    @Autowired
    public HotelApi(HotelService hotelService) {
        this.hotelService = hotelService;
    }

    @CrossOrigin(value = "http://localhost:4200")
    @PostMapping
    public Hotel addHotel(@RequestBody Hotel hotel) {

        return hotelService.addHotel(hotel);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/all")
    public List<Hotel> getAllHotels(){
        return hotelService.getAllHotels();
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/findByCity")
    public List<Hotel> findHotelsByCity(@RequestParam String cityName){
        return hotelService.findHotelsByCity(cityName);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/findById/{id}")
    public Hotel findHotelById(@PathVariable Long id){
        return hotelService.findHotelById(id);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public boolean deleteHotel(@PathVariable Long id){
        return hotelService.deleteHotel(id);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @PutMapping
    public Hotel modifyHotel(@RequestBody Hotel hotel){
        return hotelService.modifyHotel(hotel);
    }
}
