package com.globtrotter.globtrotter.api;

import com.globtrotter.globtrotter.api.model.CityApiModel;
import com.globtrotter.globtrotter.model.City;
import com.globtrotter.globtrotter.model.Country;
import com.globtrotter.globtrotter.service.CityService;
import com.globtrotter.globtrotter.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/city")
public class CityApi {

    private CityService cityService;
    private CountryService countryService;

    @Autowired
    public CityApi(CityService cityService, CountryService countryService) {

        this.cityService = cityService;
        this.countryService = countryService;
    }

    @CrossOrigin(value = "http://localhost:4200")
    @PostMapping
    public City addCity(@RequestBody CityApiModel cityApiModel) {
        Country country = countryService.findCountryById(cityApiModel.getCountryId());
        City city = new City();
        city.setId(cityApiModel.getId());
        city.setName(cityApiModel.getName());
        city.setCountry(country);
        return cityService.addCity(city);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/all")
    public List<City> findAllCities() {
        return cityService.findAllCities();
    }

    @GetMapping("/findByCountry")
    public List<City> findCitiesByCountry(@RequestParam String countryName) {
        return cityService.findCitiesByCountry(countryName);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/findById/{id}")
    public City findCityById(@PathVariable Long id) {
        return cityService.findCityById(id);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public boolean deleteCity(@PathVariable Long id) {
        return cityService.deleteCity(id);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @PutMapping
    public City modifyCity(City city) {
        return cityService.modifyCity(city);
    }
}
