package com.globtrotter.globtrotter.api;

import com.globtrotter.globtrotter.model.Continent;
import com.globtrotter.globtrotter.service.ContinentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/continent")
public class ContinentApi {

    private ContinentService continentService;

    @Autowired
    public ContinentApi(ContinentService continentService) {

        this.continentService = continentService;
    }

    @CrossOrigin(value = "http://localhost:4200")
    @PostMapping
    public Continent addContinent(@RequestBody Continent continent) {

        return continentService.addContinent(continent);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/all")
    public List<Continent> getAllContinents() {
        return continentService.getAllContinents();
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/findByName")
    public Continent findContinentsByName(@RequestParam String name) {
        return continentService.findContinentByName(name);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/findById/{id}")
    public Continent findContinentById(@PathVariable Long id) {
        return continentService.findContinentById(id);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public boolean deleteContinent(@PathVariable Long id) {

        return continentService.deleteContinent(id);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @PutMapping
    public Continent modifyContinent(@RequestBody Continent continent) {
        return continentService.modifyContinent(continent);
    }
}
