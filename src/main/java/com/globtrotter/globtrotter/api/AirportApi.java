package com.globtrotter.globtrotter.api;

import com.globtrotter.globtrotter.api.model.AirportApiModel;
import com.globtrotter.globtrotter.model.Airport;
import com.globtrotter.globtrotter.model.City;
import com.globtrotter.globtrotter.service.AirportService;
import com.globtrotter.globtrotter.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/airport")
public class AirportApi {

    private AirportService airportService;
    private CityService cityService;


    @Autowired
    public AirportApi(AirportService airportService, CityService cityService) {
        this.airportService = airportService;
        this.cityService = cityService;
    }

    @CrossOrigin(value = "http://localhost:4200")
    @PostMapping
    public Airport addAirport(@RequestBody AirportApiModel airportApiModel) {
        City city = cityService.findCityById(airportApiModel.getCityId());
        Airport airport = new Airport();
        airport.setId(airportApiModel.getId());
        airport.setName(airportApiModel.getName());
        airport.setCity(city);
        return airportService.addAirport(airport);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/all")
    public List<Airport> getAllAirports() {
        return airportService.getAllAirports();
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/findByCity")
    public List<Airport> getAirportsByCity(@RequestParam String cityName){
        return airportService.getAirportsByCity(cityName);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @GetMapping("/findById/{id}")
    public Airport getAirportById(@PathVariable Long id){
        return airportService.getAirportById(id);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @DeleteMapping("/delete/{id}")
    public boolean deleteAirport(@PathVariable Long id) {
        return airportService.deleteAirport(id);
    }

    @CrossOrigin(value = "http://localhost:4200")
    @PutMapping
    public Airport modifyAirport(AirportApiModel airportApiModel) {
        City city = cityService.findCityById(airportApiModel.getCityId());
        Airport airport = new Airport();
        airport.setId(airportApiModel.getId());
        airport.setName(airportApiModel.getName());
        airport.setCity(city);
        return airportService.addAirport(airport);
    }
}
