package com.globtrotter.globtrotter.model;

public enum FoodType {

    ONESTAR("BB"), TWOSTAR("HB"), THREESTAR("FB"), FOURSTAR("AI");

    private String description;

    FoodType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
