package com.globtrotter.globtrotter.model;

public enum Standard {

    ONESTAR("*"), TWOSTAR("**"), THREESTAR("***"), FOURSTAR("****"), FIVESTAR("*****");

    private String rate;

    Standard(String rate) {
        this.rate = rate;
    }

    public String getRate() {
        return rate;
    }
}
