package com.globtrotter.globtrotter.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "airport_from_id")
    private Airport airportFrom;

    @ManyToOne
    @JoinColumn(name = "airport_to_id")
    private Airport airportTo;

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotelTo;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City cityTo;

    private LocalDateTime departureDate;

    private LocalDateTime returnDate;

    private int countOfDays;

    @Enumerated(EnumType.STRING)
    private FoodType type;

    private double priceForAdult;

    private double priceForChild;

    private double promotion;

    private int countOfPerson;

    private String description;


}
