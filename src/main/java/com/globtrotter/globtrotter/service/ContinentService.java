package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.Continent;

import java.util.List;

public interface ContinentService {

    Continent findContinentById(Long id);

    Continent findContinentByName(String name);

    Continent addContinent(Continent continent);

    List<Continent> getAllContinents();

    boolean deleteContinent(Long id);

    Continent modifyContinent(Continent continent);

}

