package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.Airport;
import com.globtrotter.globtrotter.repository.AirportJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirportServiceSingleton implements AirportService {

    @Autowired
    private AirportJpaRepository airportJpaRepository;

    @Override
    public Airport addAirport(Airport airport) {
        return airportJpaRepository.save(airport);
    }

    @Override
    public List<Airport> getAllAirports() {
        return airportJpaRepository.findAll();
    }

    @Override
    public List<Airport> getAirportsByCity(String cityName) {
        return airportJpaRepository.getAirportsByCity(cityName);
    }

    @Override
    public Airport getAirportById(Long id) {
        return airportJpaRepository.findById(id).get();
    }

    @Override
    public boolean deleteAirport(Long id) {
        try {
            airportJpaRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }


    @Override
    public Airport modifyAirport(Airport airport) {
        return airportJpaRepository.save(airport);
    }
}
