package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.*;

import java.time.LocalDateTime;
import java.util.List;

public interface TripService {

    Trip addTrip(Trip trip);

    List<Trip> getAllTrips();
//
//    Trip getTripById(Long id);
//
    List<Trip> getTripsByDepartureAirport(String airportName);
//
////    List<Trip> getTripsByReturnAirport(Airport airport);
//////
//////    List<Trip> getTripsByHotel(Hotel hotel);
//////
//////    List<Trip> getTripsByDepartureDate(LocalDateTime departureDate);
//////
//////    List<Trip> getTripsByReturnDate(LocalDateTime returnDate);
//////
//////    List<Trip> getTripsByFoodType(FoodType type);
//////
//////    List<Trip> getTripsByHotelStandard(Standard standard);
//////
//////    List<Trip> getTripsByDuration(int countOfDays);
//////
//////    Trip modifyTrip(Trip trip);
//////
    boolean deleteTrip(Long id);
}
