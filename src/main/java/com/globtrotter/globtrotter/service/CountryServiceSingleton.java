package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.Country;
import com.globtrotter.globtrotter.repository.CountryJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceSingleton implements CountryService {


    @Autowired
    private CountryJpaRepository countryJpaRepository;

    @Override
    public Country addCountry(Country country) {

        return countryJpaRepository.save(country);
    }

    @Override
    public List<Country> getAllCountries() {
        return countryJpaRepository.findAll();
    }

    @Override
    public List<Country> findCountriesByContinent(String continentName) {
        return countryJpaRepository.findCountriesByContinent(continentName);
    }

    @Override
    public Country findCountryById(Long id) {
        return countryJpaRepository.findById(id).get();
    }

    @Override
    public boolean deleteCountry(Long id) {
        try {
            countryJpaRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public Country modifyCountry(Country country) {
        return countryJpaRepository.save(country);
    }
}
