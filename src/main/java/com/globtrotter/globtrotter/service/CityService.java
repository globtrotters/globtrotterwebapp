package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.City;

import java.util.List;

public interface CityService {

    City addCity(City city);

    List<City> findAllCities();

    List<City> findCitiesByCountry(String countryName);

    City findCityById(Long id);

    boolean deleteCity(Long id);

    City modifyCity(City city);
}
