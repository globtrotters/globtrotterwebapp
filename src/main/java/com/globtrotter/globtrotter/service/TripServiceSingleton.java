package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.*;
import com.globtrotter.globtrotter.repository.TripJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TripServiceSingleton implements TripService {

    @Autowired
    private TripJpaRepository tripJpaRepository;

    @Override
    public Trip addTrip(Trip trip) {
        return tripJpaRepository.save(trip);
    }

    @Override
    public boolean deleteTrip(Long id) {
        try {
            tripJpaRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public List<Trip> getAllTrips() {
        return tripJpaRepository.findAll();
    }
//
//    @Override
//    public Trip getTripById(Long id) {
//        return null;
//    }
//
    @Override
    public List<Trip> getTripsByDepartureAirport(String airportName) {
        return tripJpaRepository.getTripsByDepartureAirport(airportName);
    }
//
//    @Override
//    public List<Trip> getTripsByReturnAirport(Airport airport) {
//        return null;
//    }
//
//    @Override
//    public List<Trip> getTripsByHotel(Hotel hotel) {
//        return null;
//    }
//
//    @Override
//    public List<Trip> getTripsByDepartureDate(LocalDateTime departureDate) {
//        return null;
//    }
//
//    @Override
//    public List<Trip> getTripsByReturnDate(LocalDateTime returnDate) {
//        return null;
//    }
//
//    @Override
//    public List<Trip> getTripsByFoodType(FoodType type) {
//        return null;
//    }
//
//    @Override
//    public List<Trip> getTripsByHotelStandard(Standard standard) {
//        return null;
//    }
//
//    @Override
//    public List<Trip> getTripsByDuration(int countOfDays) {
//        return null;
//    }
//
//    @Override
//    public Trip modifyTrip(Trip trip) {
//        return null;
//    }
}
