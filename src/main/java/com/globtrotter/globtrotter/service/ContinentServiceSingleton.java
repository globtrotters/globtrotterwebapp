package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.Continent;
import com.globtrotter.globtrotter.repository.ContinentJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContinentServiceSingleton implements ContinentService {

    @Override
    public Continent findContinentById(Long id) {
        return continentRepository.findById(id).get();
    }

    @Autowired
    private ContinentJpaRepository continentRepository;

    @Override
    public Continent findContinentByName(String name) {
        return continentRepository.findByName(name);
    }

    @Override
    public Continent addContinent(Continent continent) {

        return continentRepository.save(continent);
    }

    @Override
    public List<Continent> getAllContinents() {

        return continentRepository.findAll();
    }

    @Override
    public boolean deleteContinent(Long id) {
        try {
            continentRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public Continent modifyContinent(Continent continent) {
        return continentRepository.save(continent);
    }
}
