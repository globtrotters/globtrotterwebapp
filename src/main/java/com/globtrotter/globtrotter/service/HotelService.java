package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.Hotel;

import java.util.List;

public interface HotelService {

    Hotel addHotel(Hotel hotel);

    List<Hotel> getAllHotels();

    List<Hotel> findHotelsByCity(String cityName);

    Hotel findHotelById(Long id);

    boolean deleteHotel(Long id);

    Hotel modifyHotel(Hotel hotel);
}
