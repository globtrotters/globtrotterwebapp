package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.Hotel;
import com.globtrotter.globtrotter.repository.HotelJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Service
public class HotelServiceSingleton implements HotelService {

    @Autowired
    private HotelJpaRepository hotelJpaRepository;

    @Override
    public Hotel addHotel(Hotel hotel) {

        return hotelJpaRepository.save(hotel);
    }

    @Override
    public List<Hotel> getAllHotels() {
        return hotelJpaRepository.findAll();
    }

    @Override
    public List<Hotel> findHotelsByCity(String cityName) {
        return hotelJpaRepository.findHotelsByCity(cityName);
    }

    @Override
    public Hotel findHotelById(Long id) {
        return hotelJpaRepository.findById(id).get();
    }

    @Override
    public boolean deleteHotel(Long id) {
        try{
            hotelJpaRepository.deleteById(id);
        } catch (Exception e){
            return false;
        }
        return true;
    }

    @Override
    public Hotel modifyHotel(Hotel hotel) {
        return hotelJpaRepository.save(hotel);
    }
}
