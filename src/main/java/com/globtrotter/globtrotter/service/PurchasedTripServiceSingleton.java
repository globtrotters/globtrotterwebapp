package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.PurchasedTrip;
import com.globtrotter.globtrotter.repository.PurchasedTripJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchasedTripServiceSingleton implements PurchasedTripService {

    @Autowired
    private PurchasedTripJpaRepository purchasedTripJpaRepository;

    @Override
    public PurchasedTrip addPurchasedTrip(PurchasedTrip purchasedTrip) {
        return purchasedTripJpaRepository.save(purchasedTrip);
    }
}
