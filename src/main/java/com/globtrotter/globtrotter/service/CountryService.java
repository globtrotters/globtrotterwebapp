package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.Country;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CountryService {


    Country addCountry(Country country);

    List<Country> getAllCountries();

    List<Country> findCountriesByContinent(String continentName);

    Country findCountryById(Long id);

    boolean deleteCountry(Long id);

    Country modifyCountry(Country country);
}
