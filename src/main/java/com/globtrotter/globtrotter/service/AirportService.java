package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.Airport;

import java.util.List;

public interface AirportService {

    Airport addAirport(Airport airport);

    List<Airport> getAllAirports();

    List<Airport> getAirportsByCity(String cityName);

    Airport getAirportById(Long id);

    boolean deleteAirport(Long id);

    Airport modifyAirport(Airport airport);
}
