package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.PurchasedTrip;

public interface PurchasedTripService {

    PurchasedTrip addPurchasedTrip(PurchasedTrip purchasedTrip);
}
