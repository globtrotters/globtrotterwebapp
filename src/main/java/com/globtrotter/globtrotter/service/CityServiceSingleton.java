package com.globtrotter.globtrotter.service;

import com.globtrotter.globtrotter.model.City;
import com.globtrotter.globtrotter.repository.CityJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceSingleton implements CityService {

    @Autowired
    private CityJpaRepository cityJpaRepository;

    @Override
    public City addCity(City city) {
        return cityJpaRepository.save(city);
    }

    @Override
    public List<City> findAllCities() {
        return cityJpaRepository.findAll();
    }

    @Override
    public List<City> findCitiesByCountry(String countryName) {
        return cityJpaRepository.findCitiesByCountry(countryName);
    }

    @Override
    public City findCityById(Long id) {
        return cityJpaRepository.findById(id).get();
    }

    @Override
    public boolean deleteCity(Long id) {
        try {
            cityJpaRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public City modifyCity(City city) {
        return cityJpaRepository.save(city);
    }
}
