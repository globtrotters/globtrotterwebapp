package com.globtrotter.globtrotter.repository;

import com.globtrotter.globtrotter.model.Continent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContinentJpaRepository extends JpaRepository<Continent, Long> {

    Continent findByName(String name);
}
