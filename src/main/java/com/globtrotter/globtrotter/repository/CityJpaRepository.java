package com.globtrotter.globtrotter.repository;

import com.globtrotter.globtrotter.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CityJpaRepository extends JpaRepository<City, Long> {

    @Query(value = "select city from com.globtrotter.globtrotter.model.City city inner join " +
            "city.country country where country.name = :countryName")
    List<City> findCitiesByCountry(@Param(value = "countryName") String countryName);
}
