package com.globtrotter.globtrotter.repository;

import com.globtrotter.globtrotter.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CountryJpaRepository extends JpaRepository<Country, Long> {

    @Query(value = "select country from com.globtrotter.globtrotter.model.Country country inner join " +
            "country.continent continent where continent.name = :continentName")
    List<Country> findCountriesByContinent(@Param(value = "continentName") String continentName);
}
