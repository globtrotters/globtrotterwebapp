package com.globtrotter.globtrotter.repository;

import com.globtrotter.globtrotter.model.City;
import com.globtrotter.globtrotter.model.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface HotelJpaRepository extends JpaRepository<Hotel, Long> {

    @Query(value = "select hotel from com.globtrotter.globtrotter.model.Hotel hotel inner join " +
            "hotel.city city where city.name = :cityName")
    List<Hotel> findHotelsByCity(@Param(value = "cityName") String cityName);

}
