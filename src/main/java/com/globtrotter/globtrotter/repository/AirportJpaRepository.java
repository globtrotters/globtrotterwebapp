package com.globtrotter.globtrotter.repository;

import com.globtrotter.globtrotter.model.Airport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AirportJpaRepository extends JpaRepository<Airport, Long> {

    @Query(value = "select airport from com.globtrotter.globtrotter.model.Airport airport inner join " +
            "airport.city city where city.name = :cityName")
    List<Airport> getAirportsByCity(@Param(value = "cityName") String cityName);
}
