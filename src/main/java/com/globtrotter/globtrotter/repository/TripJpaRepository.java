package com.globtrotter.globtrotter.repository;

import com.globtrotter.globtrotter.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface TripJpaRepository extends JpaRepository<Trip, Long> {

    @Query(value = "select trip from com.globtrotter.globtrotter.model.Trip trip inner join " +
            "trip.airportFrom airport where airport.name = :airportName")
    List<Trip> getTripsByDepartureAirport(@Param(value = "airportName") String airportName);

//    List<Trip> getTripsByReturnAirport(Airport airport);
//
//    List<Trip> getTripsByHotel(Hotel hotel);
//
//    List<Trip> getTripsByDepartureDate(LocalDateTime departureDate);
//
//    List<Trip> getTripsByReturnDate(LocalDateTime returnDate);
//
//    List<Trip> getTripsByFoodType(FoodType type);
//
//    List<Trip> getTripsByHotelStandard(Standard standard);
//
//    List<Trip> getTripsByDuration(int countOfDays);
}
