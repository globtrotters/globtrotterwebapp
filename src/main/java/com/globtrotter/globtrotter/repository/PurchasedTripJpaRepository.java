package com.globtrotter.globtrotter.repository;

import com.globtrotter.globtrotter.model.PurchasedTrip;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchasedTripJpaRepository extends JpaRepository<PurchasedTrip, Long> {

}
